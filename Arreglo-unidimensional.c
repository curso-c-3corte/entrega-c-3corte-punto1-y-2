#include <stdio.h>
#define MAXIMO 100

                  //Primer Punto / Trabajo Corte 3//

//Trabajo hecho por: Jorge Andrés Ballen Ramírez//
//Fecha de entrega máxima: 23/05/2024//
//Clase: Lenguajes de Programación 2do semestre//

int main() {
  int buscar, posicion, arreglo[88], i, n, k;

  /* El while genera un bucle hasta que el usuario ingrese un número entre 0 y
   * 100*/
  while (1) {

    /*        La lógica del código es:
    Un usuario ingresa un número entre 0 y 100, y a partir de ese número, 
    se llena secuencialmente hasta completar 88 números, es decir que si
    el usuario ingresa el número 0, el llenado se completará hasta el 
    número 87, aumentando 1 en 1 hasta el 87. El aumento va desde el número
    que ingresa. 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15...
    
    Si el usuario ingresa el número 9, el programa ira aumentando de 1 en 1
    hasta los 88 números, terminando en el índice [87], con el número 96.*/
    
    printf("\nIngrese un número entero entre 0-100:");
    scanf("%d", &n);

    if (n >= 0 && n <= MAXIMO) {

      /* Tenía la duda de si eran 88 números o si iba hasta el índice 88. 
      Lo deje con 88 números, hasta el indice 87 porque contando el 0 son
      88 números*/

      for (i = 0; i < 88; i++) {

        /* Le decimos al programa que recorra desde el índice 0 hasta
        el índice 87, usando el número ingresado por el usuario y 
        sumandole el índice, el cual va aumentando conforme avanza de
        1 en 1 */
        
        arreglo[i] = n + i;
      }

      for (i = 0; i < 88; i++) {
        printf("arreglo[%d] = %d\n", i, arreglo[i]);
      }
      break;
    }

    else
      printf("\nEl número ingresado no está en el rango permitido, vuelva a intentarlo:\n");
  }
  printf("\nIngrese el número que desea buscar en el arreglo: ");
  scanf("%d", &buscar);

  posicion = 0;

  for (i = 0; i < 88; i++) {

    if (arreglo[i] == buscar) {
      posicion = 1;
      break;
    }
  }

  /*Ahora se imprime el mensaje en caso de ser encontrado el número que quiere
  el usuario, o en caso contrario*/
  
  if (posicion == 1) {
    printf("\nEl número %d fue encontrado en la posición [%d] del arreglo", buscar, i);
  } else {
    printf("\nEl número %d NO se encontró en el arreglo", buscar);
  }
  return 0;
}