#include <stdio.h>
#define MAXIMO 100

                     //Segundo Punto / Trabajo Corte 3//

//Trabajo hecho por: Jorge Andrés Ballen Ramírez//
//Fecha de entrega máxima: 23/05/2024//
//Clase: Lenguajes de Programación 2do semestre//

int main() {
  int buscar, posicion, arreglo[9][9], i, n, j, posicion_i, posicion_j;

  /* El while genera un bucle hasta que el usuario ingrese un número entre 0 y
   * 100*/
  while (1) {

    /*        La lógica del código es:
    Un usuario ingresa un número entre 0 y 100, y a partir de ese número, 
    se llena la matriz secuencialmente hasta completar 81 números, es decir 
    que, si el usuario ingresa el número 0, el llenado se completará hasta el 
    número 80, aumentando 1 en 1 hasta el 80. El aumento va desde el número
    que ingresa. 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15...

    Si el usuario ingresa el número 9, el programa ira aumentando de 1 en 1
    hasta los 81 números, terminando en el índice [8][8], con el número 89.*/
    
    printf("\nIngrese un número entero entre 0-100:");
    scanf("%d", &n);

    if (n >= 0 && n <= MAXIMO) {

      /* Tenía la duda de si era una matriz de 81x81 o si eran 81 números.
      La hice con 81 números porque me parecia más lógico y se desbordaba
      demasiado el programa en una matriz de 81x81*/
      
      for (i = 0; i < 9; i++) {

        for (j = 0; j < 9; j++) {
          arreglo[i][j] = n++;
        }
      }

      for (i = 0; i < 9; i++) {
        for (j = 0; j < 9; j++)
          printf("arreglo[%d][%d] = %d\n", i, j, arreglo[i][j]);
      }
      break;
    }

    else
      printf("\nEl número ingresado no está en el rango permitido, vuelva a "
             "intentarlo:\n");
  }
  printf("\nIngrese el número que desea buscar en el arreglo: ");
  scanf("%d", &buscar);

  posicion = 0;

  /*A la hora de buscar el número que el usuario desea encontrar, tuve
  algunos problemas porque no daba la posición correcta, tuve que
  crear una variable para cada indice(i, j = posicion_i, posicion_j) 
  y de esa manera me empezó a funcionar*/
  
  for (i = 0; i < 9; i++) {
    for (j = 0; j < 9; j++)
      if (arreglo[i][j] == buscar) {
        posicion = 1;
        posicion_i = i;
        posicion_j = j;
        break;
      }
  }
  /*Ahora se imprime el mensaje en caso de ser encontrado el número que quiere
  el usuario, o en caso contrario*/
  if (posicion == 1) {
    printf("\nEl número %d fue encontrado en la posición [%d][%d] del arreglo",
           buscar, posicion_i, posicion_j);
  } else {
    printf("\nEl número %d NO se encontró en el arreglo", buscar);
  }
  return 0;
}